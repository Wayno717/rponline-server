﻿using Resource.CInputOutput.Data;
using System;
using System.IO;

namespace Resource.Parts
{
    public partial class ResourceAPI
    {

        public string ReadFile(ReadFileRequest request)
        {
            try
            {
                var args = new object[] { request.Value };
                if (args.Length > 0)
                {
                    var argPath = args[0].ToString();
                    var LocalPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
                    var DEFAULT_PATH = Path.Combine(LocalPath, "ProjectOnline");
                    var newPath = Path.Combine(DEFAULT_PATH, argPath);

                    string fileAndPath = newPath;
                    string currentDirectory = Path.GetDirectoryName(fileAndPath);
                    string fullPathOnly = Path.GetFullPath(currentDirectory);

                    if (!Directory.Exists(fullPathOnly))
                        return null;

                    var data = File.ReadAllText(newPath);
                    return data;
                }
                else
                {
                    Console.WriteLine("Argument count does not match expected count");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return null;
        }


        public void WriteFile(WriteFileRequest request)
        {
            try
            {
                var args = new object[] { request.Path, request.Value };
                if (args.Length > 0)
                {
                    var argPath = args[0].ToString();
                    var data = args[1].ToString();
                    var LocalPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
                    var DEFAULT_PATH = Path.Combine(LocalPath, "ProjectOnline");
                    var newPath = Path.Combine(DEFAULT_PATH, argPath);

                    string fileAndPath = newPath;
                    string currentDirectory = Path.GetDirectoryName(fileAndPath);
                    string fullPathOnly = Path.GetFullPath(currentDirectory);

                    if (!Directory.Exists(fullPathOnly))
                        Directory.CreateDirectory(fullPathOnly);
                    File.WriteAllText(newPath, data);
                }
                else
                {
                    Console.WriteLine("Argument count does not match expected count");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
    }
}
