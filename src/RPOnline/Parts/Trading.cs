﻿using Newtonsoft.Json;
using Pro.Trading.Client;

using Resource.CTrading.Data;
using System;
using System.Net.Http;

namespace Resource.Parts
{
    public partial class ResourceAPI
    {

        public void DeleteItem(CTrading.Data.DeleteItemRequest request)
        {
            try
            {
                var args = new object[] { request.Value };
                var client = new HttpClient();
                var api = new TradingAPI("http://pro-trading-webapi", client);
                var x = api.DeleteItemAsync(new Pro.Trading.Client.DeleteItemRequest()
                {
                    Name = args[0].ToString(),
                });
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public CTrading.Data.ListItemsResponse ListItem()
        {
            try
            {
                var client = new HttpClient();
                var api = new TradingAPI("http://pro-trading-webapi", client);
                var x = api.ListItemsAsync();
                return new CTrading.Data.ListItemsResponse() { Items = JsonConvert.SerializeObject(x.Result.Items) };
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return null;
        }

        public void PutItem(PutItemRequest request)
        {
            try
            {
                var args = new object[] { request.Value };
                var client = new HttpClient();
                var api = new TradingAPI("http://pro-trading-webapi", client);
                var x = api.InsertItemAsync(new InsertItemRequest()
                {
                    Item = new Item()
                    {
                        Name = args[0].ToString(),
                        Price = new Random().Next()
                    }
                });
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
    }
}
