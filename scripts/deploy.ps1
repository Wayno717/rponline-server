$targetDir = "D:\"
$name = "ProjectOnline"
$fullPath = "$targetDir\$name"
$repo = ".\"
$componentName = pwd | Select-Object | %{$_.ProviderPath.Split("\")[-1]}
$resourceDir = "$fullPath\resources\$componentName"

Write-Output $componentName
Write-Output $resourceDir
New-Item -ItemType Directory -Force -Path "$fullPath" 
if (Test-Path "$resourceDir") {
	Remove-Item "$resourceDir" -Recurse -Force
}
New-Item -ItemType Directory -Force -Path "$resourceDir" 
Copy-Item -Path ".\artifacts\*" -Destination "$resourceDir"  -Recurse