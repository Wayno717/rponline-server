﻿namespace Resource.CDebugActions.Data
{
    public class ConsoleWriteLineRequest
    {
        public string Value { get; set; }
    }
    public class ConsoleWriteRequest
    {
        public string Value { get; set; }
    }
}
