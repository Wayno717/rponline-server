﻿namespace Resource.CTrading.Data
{
    public class PutItemRequest
    {
        public string Value { get; set; }
    }
    public class DeleteItemRequest
    {
        public string Value { get; set; }
    }
    public class ListItemsRequest
    {
        public string Value { get; set; }
    }
}
