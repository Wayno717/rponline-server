﻿using Common.Logging;
using System;

namespace Resource.Parts
{
    public partial class ResourceAPI
    {

        private static readonly ILog log = Proline.Resource.Logging.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public void LogDebug(object obj)
        {
            try
            {
                // Log in memory
                log.Debug(obj.ToString());
            }
            catch (Exception)
            {

            }
        }

        public void LogWarn(object obj)
        {
            try
            {
                log.Warn(obj.ToString());
            }
            catch (Exception)
            {

            }
        }

        public void LogInfo(object obj)
        {
            try
            {
                log.Info(obj.ToString());
            }
            catch (Exception)
            {

            }
        }

        public void LogError(object obj)
        {
            try
            {
                log.Error(obj.ToString());
            }
            catch (Exception e)
            {

            }
        }
    }
}
