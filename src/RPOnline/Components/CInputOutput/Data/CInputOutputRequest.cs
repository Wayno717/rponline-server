﻿namespace Resource.CInputOutput.Data
{

    public class ReadFileRequest
    {
        public string Value { get; set; }
    }
    public class WriteFileRequest
    {
        public string Value { get; set; }
        public string Path { get; set; }
    }
}
