﻿namespace Resource.CInputOutput.Data
{
    public class ReadFileResponse
    {
        public string[] Files { get; set; }
    }
}
