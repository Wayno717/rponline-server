# dotnet-install
if (Test-Path "artifacts\") {
	Remove-Item "artifacts\" -Recurse -Force
}

dotnet restore src/
dotnet build src/ --output artifacts/ --no-restore /p:BuildNumber=999

#Copy the data in the data dir to the artificats location
if (Test-Path ".\root\") {
	Copy-Item -Path ".\root\*" -Destination "artifacts\" -Recurse
}
Remove-Item "artifacts\CitizenFX.Core.*" -Recurse -Force
